﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.AI;
using PolyPerfect;
public class Timer : MonoBehaviour
{
    public float timer;
    float startTime;
    public Image clock;
    UIManager ui;
    GameManager gm;
    Target target;
    public bool timerStop;
    void Start()
    {
        ui = FindObjectOfType<UIManager>();
        gm = ui.GetComponent<GameManager>();
        target = ui.GetComponent<Target>();
        startTime = timer;
    }

    void Update()
    {
        
    }

    public void StartTimer()
    {
        DOTween.To(() => clock.fillAmount, x => clock.fillAmount = x, 0, timer);

        StartCoroutine("startTimer");
    }
    public IEnumerator startTimer()
    {

        while(timer >= 0)
        {
            if(!timerStop)
                timer -= Time.deltaTime;

            ui.time.text = Mathf.RoundToInt(timer).ToString();

            if(timer <= 0)
            {
                ui.buttons.SetActive(false);
                ui.timer.gameObject.SetActive(false);
                ui.timerUP.gameObject.SetActive(true);
                target.character = gm.targetPerson;
                target.isTargeting = true;
                target.targetEnable = false;
                StartCoroutine(target.LookAtTarget(false));
                gm.targetPerson.GetComponent<NavMeshAgent>().isStopped = true;
                Invoke("nuke", 4);
                Invoke("OpenPanel", 5);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    void nuke()
    {
        gm.nuke.transform.position = gm.targetPerson.transform.position + gm.targetPerson.transform.forward * -10;
        gm.nuke.Play();
        foreach(WanderScript ws in FindObjectsOfType<WanderScript>())
        {
            if(gm.targetPerson.GetComponent<WanderScript>() != ws)
                ws.Die();
        }
    }

    void OpenPanel()
    {
        ui.timeUpPanel.gameObject.SetActive(true);
    }

    public void ResetTimer()
    {
        timer = startTime;
    }

    public void NotStopTimer()
    {
        timerStop = false;
    }

    public void StopTimer()
    {
        timerStop = true;
    }
}
