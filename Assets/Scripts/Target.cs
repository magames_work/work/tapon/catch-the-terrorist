﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.PostProcessing;
using UnityEngine.EventSystems;
public class Target : MonoBehaviour
{
    public CharactersManager charactersManager;
    public float offset;
    private UIManager ui;
    private GameManager gm;
    private Timer timer;
    private Transform charParent;
    [HideInInspector]
    public GameObject character;

    private Vector3 defaultPos, defaultRot;
    [HideInInspector]
    public bool isTargeting, targetEnable;
    private Ray ray;
    private RaycastHit hit;
    private void Start() {
        charParent = charactersManager.CharactersParent;
        defaultPos = Camera.main.transform.position;
        defaultRot = Camera.main.transform.rotation.eulerAngles;
        isTargeting = false;
        targetEnable = true;

        ui = FindObjectOfType<UIManager>();
        gm = ui.GetComponent<GameManager>();
        timer = ui.GetComponent<Timer>();

        StartCoroutine(MuteAnimations());
    }

    private void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(targetEnable && !isTargeting && Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000) && hit.transform.tag == "Character" && !EventSystem.current.IsPointerOverGameObject())
        {
            character = hit.transform.gameObject;
        }

        if(targetEnable && !isTargeting && Input.GetMouseButtonUp(0) && character == hit.transform.gameObject && Physics.Raycast(ray, out hit, 1000) && hit.transform.tag == "Character" && !EventSystem.current.IsPointerOverGameObject())
        {
            character = hit.transform.gameObject;
            TargetToCharacter();
            isTargeting = true;
            targetEnable = false;
        }
        else if(targetEnable && isTargeting && Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            ReturnToDefaultCam();
            isTargeting = false;
            character = null;
        }
    }

    private void TargetToCharacter()
    {
        StartCoroutine(LookAtTarget(true));
        character.GetComponent<NavMeshAgent>().isStopped = true;
    }

    DG.Tweening.Core.TweenerCore<System.Single, System.Single, DG.Tweening.Plugins.Options.FloatOptions> fov;
    DG.Tweening.Core.TweenerCore<UnityEngine.Vector3, UnityEngine.Vector3, DG.Tweening.Plugins.Options.VectorOptions> move;

    public IEnumerator LookAtTarget(bool openButtons)
    {
        Camera.main.transform.DOLookAt(new Vector3(character.transform.position.x , 1.5f, character.transform.position.z), 1f);

        yield return new WaitForSeconds(1);

        move = Camera.main.transform.DOMove(character.transform.position + character.transform.forward * 2 + Vector3.up * 1.6f, 1f);

        fov = DOTween.To(() => Camera.main.fieldOfView, x => Camera.main.fieldOfView = x, 70f, 2f);

        float catchTime = Time.time;

        while(isTargeting)
        {
            Camera.main.transform.LookAt(new Vector3(character.transform.position.x , 1.2f, character.transform.position.z) + character.transform.right * offset);
            
            if(catchTime + 1 < Time.time)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }
        
        if(openButtons)
        {
            Camera.main.GetComponent<PostProcessingBehaviour>().enabled = true;
            //DisableCharacters();
            ui.buttons.gameObject.SetActive(true);
            targetEnable = true;
        }
    }

    private IEnumerator MuteAnimations()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.01f);

            if(isTargeting)
            {
                character.GetComponent<Animator>().SetBool("isWalking", false);
                character.GetComponent<Animator>().SetBool("isWaving", false);
                character.GetComponent<Animator>().SetBool("isTexting", false);
            }
        }
    }

    public void arest()
    {
        if(gm.targetPerson == character)
        {
            ui.arested.gameObject.SetActive(true);
            gm.confetti.Play();
            gm.confetti.gameObject.SetActive(true);
            ui.buttons.SetActive(false);
            targetEnable = false;
            timer.StopTimer();
        }
        else
        {
            ui.wrongPersonPanel.gameObject.SetActive(true);
            ui.buttons.SetActive(false);
            targetEnable = false;
        }
    }

    public void ReturnToDefaultCam()
    {
        fov.Complete();
        Camera.main.fieldOfView = 27;
        ui.blur.DOFade(0, 0);
        ui.buttons.gameObject.SetActive(false);
        Camera.main.GetComponent<PostProcessingBehaviour>().enabled = false;
        character.GetComponent<NavMeshAgent>().isStopped = false;
        Camera.main.transform.position = defaultPos;
        Camera.main.transform.eulerAngles = defaultRot;
        EnableCharacters();
    }

    private void DisableCharacters()
    {
        foreach(Transform child in charParent)
        {
            if(child != character.transform)
            {
                child.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

    private void EnableCharacters()
    {
        foreach(Transform child in charParent)
        {
            if(child != character.transform)
            {
                child.GetChild(0).gameObject.SetActive(true);
            }
        }
    }
}
