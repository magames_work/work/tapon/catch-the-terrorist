﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public GameObject buttons;
    public Image clownTask, blurImg, timeUpPanel, wrongPersonPanel, arested, timer, timerUP;
    public Text time;
    public Material blur;

    Image _img;
    public void fadeOut(Image img)
    {
        _img = img;
        _img.DOFade(0, 1);
        Invoke("disableImg", 1);
    }
    public void fadeIn(Image img)
    {
        _img = img;
        _img.gameObject.SetActive(true);
        _img.DOFade(1, 1);
    }
    private void disableImg()
    {
        _img.gameObject.SetActive(false);
    }

}
