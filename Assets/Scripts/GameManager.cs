﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public GameObject targetPerson;
    public ParticleSystem nuke, confetti;

    public void loadLvl(int id)
    {
        SceneManager.LoadScene(id);
    }
}
