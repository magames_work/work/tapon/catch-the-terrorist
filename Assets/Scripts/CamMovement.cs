﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovement : MonoBehaviour
{
    public float speed;
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            Vector3 movementX = new Vector3(0, 0f, -Input.GetAxis("Mouse X"));
            //Vector3 movementY = new Vector3(Input.GetAxis("Mouse Y"), 0, 0f);
            if(cam.transform.position.z + movementX.z * Time.deltaTime * speed < -7.48f & cam.transform.position.z + movementX.z * Time.deltaTime * speed > -12.65f)
            {
                cam.transform.position += movementX * Time.deltaTime * speed;
                //cam.transform.position += movementY * Time.deltaTime * speed;
            }
            
        }
    }
}
